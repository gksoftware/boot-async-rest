Sample project to demonstrate the use of Spring Boot with rxjava asynchronous request handling.

# Getting Started
To run the project, simply execute the BootAsyncRestApplication class. 

## Build from maven
mvn clean install

## Run from maven
This will start up the spring boot application. If you are using maven then you can execute the following command:
mvn spring-boot:run

## Run from the command line
After building the project, it can be run as follows:
java -jar target/boot-async-rest-1.0.0.jar

## Changing server port
The server will bind to port 8080 by default. 

### Configure using yaml
It can be changed by updating application.yml with the following parameter:
server.port: 8081

### Configure using vm parameter
Or it can be changed by setting the following vm parameter at execution time:
-Dserver.port=8081


# Consuming the API
To consume the API, there is a postman collection that can be imported into your local postman and used to
execute requests against the API.

The colletion is named as follows and can be found in the root of the project.
AsyncRest.postman_collection.json


