package io.gksoftware.example.asyncrest.blog.standard;

import io.gksoftware.example.asyncrest.blog.standard.StandardBlog;
import io.gksoftware.example.asyncrest.blog.standard.StandardBlogRepository;
import io.gksoftware.example.asyncrest.blog.standard.StandardBlogService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class StandardBlogServiceTest {

   @Mock
   private StandardBlogRepository standardBlogRepository;

   @Mock
   private StandardBlog standardBlog;

   @InjectMocks
   private StandardBlogService blogServiceAsync;

   @Test
   public void testFindBlogShouldSucceed() {
      final String testId = "TestId";
      when(standardBlogRepository.get(eq(testId))).thenReturn(Optional.of(standardBlog));

      blogServiceAsync.findBlog(testId)
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertNoTimeout()
            .awaitTerminalEvent();

      verify(standardBlogRepository).get(eq(testId));
   }

   @Test
   public void testFindBlogShouldReturnNotFoundError() {
      final String testId = "TestId";
      when(standardBlogRepository.get(eq(testId))).thenReturn(Optional.empty());

      blogServiceAsync.findBlog(testId)
            .test()
            .assertError(ResponseStatusException.class)
            .assertError(ex -> {
               ResponseStatusException exception = (ResponseStatusException)ex;
               return exception.getStatus() == HttpStatus.NOT_FOUND;
            })
            .assertNoTimeout()
            .awaitTerminalEvent();

      verify(standardBlogRepository).get(eq(testId));
   }

   @Test
   public void testFindBlogShouldReturnThrowInternalServiceError() {
      final String testId = "TestId";
      when(standardBlogRepository.get(eq(testId))).thenThrow(new IllegalStateException("Test exception"));

      blogServiceAsync.findBlog(testId)
            .test()
            .assertError(ResponseStatusException.class)
            .assertError(ex -> {
               ResponseStatusException exception = (ResponseStatusException)ex;
               return exception.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR;
            })
            .assertNoTimeout()
            .awaitTerminalEvent();

      verify(standardBlogRepository).get(eq(testId));
   }

   @Test
   public void testSaveBlogShouldSucceed() {
      when(standardBlogRepository.save(eq(standardBlog))).thenReturn(standardBlog);

      blogServiceAsync.saveBlog(standardBlog)
            .test()
            .assertComplete()
            .assertValue(b -> b == standardBlog)
            .assertNoErrors()
            .assertNoTimeout()
            .awaitTerminalEvent();

      verify(standardBlogRepository).save(eq(standardBlog));
   }

   @Test
   public void testSaveBlogShouldThrowInternalServiceError() {
      when(standardBlogRepository.save(eq(standardBlog))).thenThrow(new IllegalStateException("Test exception"));

      blogServiceAsync.saveBlog(standardBlog)
            .test()
            .assertError(ResponseStatusException.class)
            .assertError(ex -> {
               ResponseStatusException exception = (ResponseStatusException)ex;
               return exception.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR;
            })
            .assertNoTimeout()
            .awaitTerminalEvent();

      verify(standardBlogRepository).save(eq(standardBlog));
   }
}
