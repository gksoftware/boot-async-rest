package io.gksoftware.example.asyncrest.blog.standard;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import io.gksoftware.example.asyncrest.blog.standard.StandardBlog;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;

@SpringBootTest
public class StandardBlogTest {

   @Test
   public void testEqualsByRefShouldSucceed() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      assertThat(a, is(a));
   }

   @Test
   public void testEqualsShoudldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final Object b = new Object();
      assertThat(a, not(b));
   }

   @Test
   public void testEqualsWithNullShoudldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      assertThat(a.equals(null), is(false));
   }

   @Test
   public void testEqualsByValueShouldSucceed() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id", "author", "title", "content", now);
      assertThat(a, is(b));
   }

   @Test
   public void testEqualsByNullIdShouldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog(null, "author", "title", "content", now);
      assertThat(a, not(b));
   }
   @Test
   public void testEqualsByNullIdOtherShouldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog(null, "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id", "author", "title", "content", now);
      assertThat(a, not(b));
   }

   @Test
   public void testEqualsMismatchedIdShouldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id2", "author", "title", "content", now);
      assertThat(a, not(b));
   }

   @Test
   public void testEqualsMismatchedAuthorShouldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id", "author2", "title", "content", now);
      assertThat(a, not(b));
   }

   @Test
   public void testEqualsMismatchedTitleShouldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id", "author", "title2", "content", now);
      assertThat(a, not(b));
   }

   @Test
   public void testEqualsMismatchedContentShouldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id", "author", "title", "content2", now);
      assertThat(a, not(b));
   }
   @Test
   public void testEqualsMismatchedDateShouldFail() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id", "author", "title", "content", now.plus(10, SECONDS));
      assertThat(a, not(b));
   }

   @Test
   public void testEqualsWithNullIdByValueShouldSucceed() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog(null, "author", "title", "content", now);
      final StandardBlog b = new StandardBlog(null, "author", "title", "content",now);
      assertThat(a, is(b));
   }

   @Test
   public void testHashCodeShouldSucceed() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog("id", "author", "title", "content", now);
      final StandardBlog b = new StandardBlog("id", "author", "title", "content", now);
      assertThat(a.hashCode(), is(b.hashCode()));
   }

   @Test
   public void testHashCodeWithNullIdShouldSucceed() {
      final Instant now = Instant.now();
      final StandardBlog a = new StandardBlog(null, "author", "title", "content", now);
      final StandardBlog b = new StandardBlog(null, "author", "title", "content", now);
      assertThat(a.hashCode(), is(b.hashCode()));
   }
}
