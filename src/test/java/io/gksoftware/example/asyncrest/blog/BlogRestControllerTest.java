package io.gksoftware.example.asyncrest.blog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.gksoftware.example.asyncrest.blog.standard.StandardBlog;
import io.gksoftware.example.asyncrest.blog.standard.StandardBlogService;
import io.reactivex.Single;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(BlogRestController.class)
public class BlogRestControllerTest {

   @Autowired
   private MockMvc mockMvc;

   @MockBean
   private StandardBlogService blogService;

   private static ObjectMapper objectMapper;

   @BeforeAll
   public static void setup() {
      objectMapper = new ObjectMapper();
      objectMapper.registerModule(new JavaTimeModule());
      objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
   }

   @Test
   public void testGetShouldReturnSuccess() throws Exception {
      final String testId = "testId";
      final StandardBlog expected = new StandardBlog("id", "author", "title", "content", Instant.now());
      when(blogService.findBlog(eq(testId))).thenReturn(Single.just(expected));

      final MvcResult mvcResult = mockMvc.perform(get("/blog/testId")
            .accept(MediaType.APPLICATION_JSON)).andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is("id")))
            .andExpect(jsonPath("$.author", is("author")))
            .andExpect(jsonPath("$.title", is("title")))
            .andExpect(jsonPath("$.content", is("content")))
            .andExpect(jsonPath("$.date", notNullValue()));

      verify(blogService).findBlog(eq(testId));
   }

   @Test
   public void testGetShouldReturnNotFound() throws Exception {
      final String testId = "testId";
      when(blogService.findBlog(eq(testId)))
            .thenReturn(Single.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found")));

      final MvcResult mvcResult = mockMvc.perform(get("/blog/testId")
            .accept(MediaType.APPLICATION_JSON)).andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
            .andExpect(status().isNotFound());

      verify(blogService).findBlog(eq(testId));
   }

   @Test
   public void testGetShouldReturnInternalServiceError() throws Exception {
      final String testId = "testId";
      when(blogService.findBlog(eq(testId))).thenReturn(
            Single.error(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error")));

      final MvcResult mvcResult = mockMvc.perform(get("/blog/testId")
            .accept(MediaType.APPLICATION_JSON)).andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
            .andExpect(status().isInternalServerError());

      verify(blogService).findBlog(eq(testId));
   }

   @Test
   public void testPostShouldCreateAndReturnSuccess() throws Exception {
      final StandardBlog expected = new StandardBlog("id", "author", "title", "content", Instant.now());
      when(blogService.saveBlog(any())).thenReturn(Single.just(expected));

      final MvcResult mvcResult = mockMvc.perform(post("/blog")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .characterEncoding("UTF-8")
            .content(objectMapper.writeValueAsString(expected)))
            .andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is("id")))
            .andExpect(jsonPath("$.author", is("author")))
            .andExpect(jsonPath("$.title", is("title")))
            .andExpect(jsonPath("$.content", is("content")))
            .andExpect(jsonPath("$.date", notNullValue()));

      verify(blogService).saveBlog(any());
   }

   @Test
   public void testPostShouldFailAndReturnInternalServiceError() throws Exception {
      final StandardBlog expected = new StandardBlog("id", "author", "title", "content", Instant.now());
      when(blogService.saveBlog(any())).thenReturn(Single.error(
            new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Service Error")));

      final MvcResult mvcResult = mockMvc.perform(post("/blog")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .characterEncoding("UTF-8")
            .content(objectMapper.writeValueAsString(expected)))
            .andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
            .andExpect(status().isInternalServerError());

      verify(blogService).saveBlog(any());
   }

}
