package io.gksoftware.example.asyncrest.blog.standard;

import io.gksoftware.example.asyncrest.blog.standard.StandardBlog;
import io.gksoftware.example.asyncrest.blog.standard.StandardBlogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;

import static io.gksoftware.example.asyncrest.blog.standard.StandardBlogRepository.MAX_ATTEMPTS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
public class StandardBlogRepositoryTest {

   @Mock
   private Map<String, StandardBlog> standardBlogMap;

   @Mock
   private StandardBlog standardBlog;

   @InjectMocks
   private StandardBlogRepository standardBlogRepository;

   @BeforeEach
   public void setup() {
      standardBlogRepository = new StandardBlogRepository(standardBlogMap);
   }

   @Test
   public void testDefaultConstructor() {
      new StandardBlogRepository();
   }

   @Test
   public void testGetShouldSucceed() {
      final String testId = "TestId";
      when(standardBlogMap.get(eq(testId))).thenReturn(standardBlog);

      final Optional<StandardBlog> result = standardBlogRepository.get(testId);
      assertThat("Unexpected null", result, is(notNullValue()));
      assertThat("Unexpected null optional", result.isPresent(), is(true));
      assertThat("Unexpected result", result.orElse(null), is(standardBlog));

      verify(standardBlogMap).get(eq(testId));
   }

   @Test
   public void testGetNullShouldSucceed() {
      final String testId = "TestId";
      when(standardBlogMap.get(eq(testId))).thenReturn(null);

      final Optional<StandardBlog> result = standardBlogRepository.get(testId);
      assertThat("Unexpected null", result, is(notNullValue()));
      assertThat("Unexpected null optional", result.isPresent(), is(false));

      verify(standardBlogMap).get(eq(testId));
   }

   @Test
   public void testSaveShouldCreateAndSucceed() {
      when(standardBlog.getId()).thenReturn(null);
      when(standardBlogMap.computeIfAbsent(any(), any())).thenReturn(standardBlog);

      StandardBlogRepository spy = spy(standardBlogRepository);
      final StandardBlog result = spy.save(standardBlog);
      assertThat("Unexpected null", result, is(notNullValue()));
      assertThat("Unexpected null optional", result, is(standardBlog));

      verify(standardBlog).getId();
      verify(spy).createBlog(eq(standardBlog));
      verify(standardBlogMap).computeIfAbsent(any(), any());
   }

   @Test()
   public void testSaveShouldCreateAndThrowException() {
      when(standardBlog.getId()).thenReturn(null);
      when(standardBlogMap.computeIfAbsent(any(), any())).thenReturn(null);

      StandardBlogRepository spy = spy(standardBlogRepository);
      Assertions.assertThrows(IllegalStateException.class, () -> {
         spy.save(standardBlog);
      });

      verify(standardBlog).getId();
      verify(spy).createBlog(eq(standardBlog));
      verify(standardBlogMap, times(MAX_ATTEMPTS)).computeIfAbsent(any(), any());
   }

   @Test
   public void testSaveShouldUpdateAndSucceed() {
      final String testId = "TestId";
      when(standardBlog.getId()).thenReturn(testId);
      when(standardBlogMap.computeIfPresent(eq(testId), any())).thenReturn(standardBlog);

      StandardBlogRepository spy = spy(standardBlogRepository);
      final StandardBlog result = spy.save(standardBlog);
      assertThat("Unexpected null", result, is(notNullValue()));
      assertThat("Unexpected null optional", result, is(standardBlog));

      verify(standardBlog, times(2)).getId();
      verify(spy).updateBlog(eq(standardBlog));
      verify(standardBlogMap).computeIfPresent(eq(testId), any());
   }

   @Test
   public void testSaveShouldUpdateAndThrowException() {
      final String testId = "TestId";
      when(standardBlog.getId()).thenReturn(testId);
      when(standardBlogMap.computeIfPresent(eq(testId), any())).thenReturn(null);

      StandardBlogRepository spy = spy(standardBlogRepository);
      Assertions.assertThrows(IllegalStateException.class, () -> {
         spy.save(standardBlog);
      });

      verify(standardBlog, times(2)).getId();
      verify(spy).updateBlog(eq(standardBlog));
      verify(standardBlogMap).computeIfPresent(eq(testId), any());
   }

   @Test
   public void testSaveWithoutMocksShouldUpdateAndSucceed() {

      final StandardBlog initialBlog = new StandardBlog(null, "author", "title", "content", Instant.now());

      final StandardBlogRepository standardBlogRepository = new StandardBlogRepository();
      StandardBlogRepository spy = spy(standardBlogRepository);

      final StandardBlog created = spy.save(initialBlog);
      assertThat(created.getId(), notNullValue());
      assertThat(created.getAuthor(), is(initialBlog.getAuthor()));
      assertThat(created.getTitle(), is(initialBlog.getTitle()));
      assertThat(created.getContent(), is(initialBlog.getContent()));
      assertThat(created.getDate(), notNullValue());

      final StandardBlog updatedBlog = new StandardBlog(created.getId(), "author2", "title2", "content2", Instant.now());
      final StandardBlog updated = spy.save(updatedBlog);
      assertThat(updated.getId(), is(updatedBlog.getId()));
      assertThat(updated.getAuthor(), is(updatedBlog.getAuthor()));
      assertThat(updated.getTitle(), is(updatedBlog.getTitle()));
      assertThat(updated.getContent(), is(updatedBlog.getContent()));
      assertThat(updated.getDate(), notNullValue());

      verify(spy, times(2)).save(any());
      verify(spy).createBlog(eq(initialBlog));
      verify(spy).updateBlog(eq(updatedBlog));
   }

}
