package io.gksoftware.example.asyncrest;

import io.reactivex.Flowable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@SpringBootApplication
public class BootAsyncRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootAsyncRestApplication.class, args);
	}



}
