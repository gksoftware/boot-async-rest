package io.gksoftware.example.asyncrest.blog.standard;

import io.gksoftware.example.asyncrest.blog.BlogRepository;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class StandardBlogRepository implements BlogRepository<String, StandardBlog> {

   private final Map<String, StandardBlog> blogMap;
   public static final int MAX_ATTEMPTS = 10;

   /**
    * Implementation of {@link BlogRepository} backed by a concurrent hash map.
    */
   public StandardBlogRepository() {
      blogMap = new ConcurrentHashMap<>();
   }

   public StandardBlogRepository(final Map<String, StandardBlog> map) {
      blogMap = map;
   }

   /**
    * Retrieve the {@link StandardBlog} based on the identifier provided.
    * @param id - representing the identiy of the blog.
    * @return optional containing the blog details.
    */
   @Override
   public Optional<StandardBlog> get(final String id) {
      return Optional.ofNullable(blogMap.get(id));
   }

   /**
    * Save the state of the {@link StandardBlog} to the persistent store. If the
    * {@link StandardBlog} contains an id it will be updated. Otherwise, a new entry
    * will be created in the persistent store.
    * @param blog StandardBlog
    * @return StandardBlog containing the most up-to-date state.
    * @throws IllegalStateException if any exception occurs during persistentce.
    */
   @Override
   public StandardBlog save(final StandardBlog blog) {
      final String id = blog.getId();
      if (id == null) {
         return createBlog(blog);
      } else {
         return updateBlog(blog);
      }
   }

   /**
    * Create and persist a new {@link StandardBlog} item. The simple approach here is to
    * generate a unique id, and attempt to ensure its unique in the persistent store. If the
    * id is already present in the persistent store, we try a finite number of times before
    * we fail the operation.
    * @param blog StandardBlog representing the blog to be created.
    * @return StandardBlog the newly created blog with the generate id.
    * @throws IllegalStateException if the oepration fails.
    */
   public StandardBlog createBlog(final StandardBlog blog) {
      int attempt = 0;
      while (attempt++ < MAX_ATTEMPTS) {
         final String id = UUID.randomUUID().toString();

         final StandardBlog created = blogMap.computeIfAbsent(id, key ->
               new StandardBlog(key, blog.getAuthor(), blog.getTitle(), blog.getContent(), Instant.now())
         );

         if (created != null) {
            return created;
         }
      }

      throw new IllegalStateException(String.format("Failed to create blog after %d attempts", MAX_ATTEMPTS));
   }

   /**
    * Update the {@link StandardBlog} item if it exists. If the update operation is successful, it will return
    * the updated {@link StandardBlog}.
    * @param blog StandardBlog
    * @return the updated blog.
    * @throws IllegalStateException if update operation was not successful.
    */
   public StandardBlog updateBlog(final StandardBlog blog) {
      final StandardBlog updated = blogMap.computeIfPresent(blog.getId(), (key, value) ->
            new StandardBlog(blog.getId(), blog.getAuthor(), blog.getTitle(), blog.getContent(), Instant.now())
      );

      if (updated == null) {
         throw new IllegalStateException("Failed to update blog");
      }

      return updated;
   }

}
