package io.gksoftware.example.asyncrest.blog.standard;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.gksoftware.example.asyncrest.blog.Blog;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.Instant;

public class StandardBlog implements Blog<String>, Serializable {

   private final String id;

   @NotBlank(message = "Author is mandatory")
   private final String author;

   @NotBlank(message = "Title is mandatory")
   private final String title;

   @NotBlank(message = "Content is mandatory")
   private final String content;

   private final Instant date;

   /**
    * Construct a new, immutable representation of a {@link Blog} resource.
    * @param id - String identity
    * @param author - String author
    * @param title - String title
    * @param content - String content
    * @param date - creation or last update date.
    */
   @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
   public StandardBlog(final @JsonProperty("id") String id,
                       final @JsonProperty("author") String author,
                       final @JsonProperty("title") String title,
                       final @JsonProperty("content") String content,
                       final @JsonProperty("date") Instant date) {
      this.id = id;
      this.author = author;
      this.title = title;
      this.content = content;
      this.date = date;
   }

   /**
    * Access the identifier.
    * @return String
    */
   public String getId() {
      return id;
   }

   /**
    * Access the author.
    * @return String
    */
   public String getAuthor() {
      return author;
   }

   /**
    * Access the title.
    * @return String
    */
   public String getTitle() {
      return title;
   }

   /**
    * Access the content.
    * @return String
    */
   public String getContent() {
      return content;
   }

   /**
    * Access the date. This is either the creation date, if the
    * object has not been updated since creation, or the last updated time.
    * @return Instant
    */
   public Instant getDate() {
      return date;
   }

   /**
    * Standard equals method.
    * @param o Object
    * @return true if the objects are considered equal.
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      StandardBlog blog = (StandardBlog) o;

      if (getId() != null ? !getId().equals(blog.getId()) : blog.getId() != null) return false;
      if (!getAuthor().equals(blog.getAuthor())) return false;
      if (!getTitle().equals(blog.getTitle())) return false;
      if (!getContent().equals(blog.getContent())) return false;
      return getDate().equals(blog.getDate());

   }

   /**
    * Standard hash code method.
    * @return int representing hash code.
    */
   @Override
   public int hashCode() {
      int result = getId() != null ? getId().hashCode() : 0;
      result = 31 * result + getAuthor().hashCode();
      result = 31 * result + getTitle().hashCode();
      result = 31 * result + getContent().hashCode();
      result = 31 * result + getDate().hashCode();
      return result;
   }
}
