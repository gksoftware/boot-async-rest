package io.gksoftware.example.asyncrest.blog;

import io.gksoftware.example.asyncrest.blog.standard.StandardBlog;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class BlogRestController {

   private final BlogService<String, StandardBlog> blogService;

   public BlogRestController(final BlogService<String, StandardBlog> blogService) {
      this.blogService = blogService;
   }

   @GetMapping("/blog/{id}")
   public Single<ResponseEntity> getBlogEntry(@PathVariable final String id) {
      return blogService.findBlog(id)
            .subscribeOn(Schedulers.io())
            .map(ResponseEntity::ok);
   }

   @PostMapping("/blog")
   public Single<ResponseEntity> postBlogEntry(final @Valid @RequestBody StandardBlog blog) {
      return blogService.saveBlog(blog)
            .subscribeOn(Schedulers.io())
            .map(ResponseEntity::ok);
   }
}
