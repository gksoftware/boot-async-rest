package io.gksoftware.example.asyncrest.blog.standard;

import io.gksoftware.example.asyncrest.blog.BlogRepository;
import io.gksoftware.example.asyncrest.blog.BlogService;
import io.reactivex.Single;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class StandardBlogService implements BlogService<String, StandardBlog> {

   private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(StandardBlogService.class);

   private final BlogRepository<String, StandardBlog> blogRepository;

   public StandardBlogService(final BlogRepository<String, StandardBlog> blogRepository) {
      this.blogRepository = blogRepository;
   }

   /**
    * Retrieve the {@link StandardBlog} with the given identifier. If the resource
    * does not exist, an http status 404 will be returned to the client. Otherwise, the
    * resource is returned to the client.
    * <strong>Note: this method is configured to be executed in an asynchronous fashion.</strong>
    * @param id representing the identifier of the {@link StandardBlog} resource to retrieve.
    * @return a reactive stream containing the blog resource representation requested, or an error if
    *    the resource was not found.
    */
   public Single<StandardBlog> findBlog(final String id) {
      return Single.create(subscriber -> {
         try {
            final Optional<StandardBlog> blog = blogRepository.get(id);
            if (blog.isPresent()) {
               subscriber.onSuccess(blog.get());
            } else {
               final String advice = String.format("Blog not found with id %s", id);
               subscriber.onError(new ResponseStatusException(HttpStatus.NOT_FOUND, advice));
            }
         } catch (Exception e) {
            final String advice = String.format("Unexpected error occurred while retrieving blog with id: %s", id);
            LOG.error(advice, e);
            subscriber.onError(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, advice, e));
         }
      });
   }

   /**
    * Persist the {@link StandardBlog} resource in the backend persistence store. If the resource already
    * exists, it will be updated. Otherwise a new resource will be created.
    * <strong>Note: this method is configured to be executed in an asynchronous fashion.</strong>
    * @param blog representing the {@link StandardBlog} resource to update updated/created.
    * @return a reactive stream containing the updated/created resource representation, or an error if there
    *   was any exception during persistence.
    */
   public Single<StandardBlog> saveBlog(final StandardBlog blog) {
      return Single.create(subscriber -> {
         try {
            final StandardBlog upserted = blogRepository.save(blog);
            subscriber.onSuccess(upserted);
         } catch (Exception e) {
            final String advice = "Unexpected error occurred while saving blog";
            LOG.error(advice, e);
            subscriber.onError(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, advice, e));
         }
      });
   }
}
