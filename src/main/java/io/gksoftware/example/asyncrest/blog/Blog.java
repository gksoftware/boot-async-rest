package io.gksoftware.example.asyncrest.blog;

import java.time.Instant;

public interface Blog<ID> {

   /**
    * Access the identity of the blog.
    * @return ID
    */
   ID getId();

   /**
    * Access the author of the blog.
    * @return String
    */
   String getAuthor();

   /**
    * Access the title of the blog.
    * @return String
    */
   String getTitle();

   /**
    * Access the content of the blog.
    * @return String
    */
   String getContent();

   /**
    * Access the date of the blog. We'll make an assumption
    * here that its the last update date of the blog article.author
    * @return Instant
    */
   Instant getDate();

}
