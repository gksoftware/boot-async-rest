package io.gksoftware.example.asyncrest.blog;

import io.reactivex.Single;

public interface BlogService<ID, T extends Blog> {

   /**
    * Access the resource with the given identifier. If a resource is found, it will
    * be returned in the asynchronous response. If no resource is found, an error will
    * be returned in the asynchronous response.
    * @param id representing the identify of the resource.
    * @return the resource representing the resource identifie provided.
    */
   Single<T> findBlog(final ID id);

   /**
    * Persist the resource provided in the persistence store. If the operation is successful, the
    * updated resource will be returned in the asynchronous response. If any persistence error occurs
    * during the persist operation, an errror will be returned in the asynchronous response.
    * @param blog - representation of the resource to be persisted.
    * @return an asynchronous response containing the resource following the persistence operation.
    */
   Single<T> saveBlog(final T blog);
}
