package io.gksoftware.example.asyncrest.blog;

import java.util.Optional;

public interface BlogRepository<ID, T extends Blog> {

   /**
    * Locate and return the blog resource based on the identifier provided.
    * If no blog is found return an empty optional.
    * @param id - identifier of the blog.
    * @return optional containing the blog if located.
    */
   Optional<T> get(ID id);

   /**
    * Persist the blog resource provided. If a blog with given identifier does not
    * exist, it will be created. Otherwise, it will be updated.
    * @param blog resource to be persisted.
    * @return created or updated blog resource.
    */
   T save(T blog);

}
